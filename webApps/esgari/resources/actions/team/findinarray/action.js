define(['vb/action/action'], (Action) => {
  'use strict';

  class CustomAction extends Action {
    perform(parameters) {
      var retArray = parameters.array.find(record => record[parameters.fieldname] === parameters.filter);
      if (retArray) {
        return Action.createSuccessOutcome({ "result": retArray });

      } else {
        return Action.createSuccessOutcome({ "result": [] });

      }
      console.log(parameters,"kkkkkkkkkkkkkkk");
    }
  }

  return CustomAction;
});